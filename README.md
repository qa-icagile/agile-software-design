# About this project
This repo contains the code bundle needed to run the QAICPASD (ICAgile Agile Doftware Design) course.

This course is intended to be language agnostic but code and examples are provided in Java.