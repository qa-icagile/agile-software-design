import java.util.Date;

public class Employee {
	// we would have getters/setters for these - omitted for simplicity
	private String empId;
	private String firstName, lastName;
	private String address;
	private Date dateOfJoining;
	private String jobTitle;

	public boolean isPromotionDueThisYear() {
		// logic to do promotion
		return true;
	}

	public Double calcIncomeTaxForCurrentYear() {
		// logic to do income tax
		return 0.0;
	}
}
