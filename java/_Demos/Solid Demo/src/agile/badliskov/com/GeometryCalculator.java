package agile.badliskov.com;

// First try at Geometry calculator without Open/Closed principle
public class GeometryCalculator {
	public double Area(Rectangle[] shapes)
    {
        double area = 0;
        for(Rectangle r : shapes)
        {
            area += r.getWidth()*r.getHeight();
        }
        return area;
    }
	
	// We also want to calculate area of circle
	public double Area(Circle[] shapes)
    {
        double area = 0;
        for(Circle r : shapes)
        {
            area += Math.PI * Math.pow(r.getRadius(),2);
        }
        return area;
    }
	
}
