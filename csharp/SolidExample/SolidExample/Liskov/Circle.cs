using System;
namespace SolidExample.Liskov
{


public class Circle : Shape {

	private double radius;

	public double Radius { get => radius; set => radius = value; }

	
	
	public override double Area=> Math.PI * Math.Pow(Radius, 2);
	

}
}