using System;
namespace SolidExample.Liskov
{

	public class GeometryCalculator {
		public double Area(Shape[] shapes) {
			double area = 0;
			foreach (Shape s in shapes) {
				area += s.Area;
			}
			return area;
		}
	}
}