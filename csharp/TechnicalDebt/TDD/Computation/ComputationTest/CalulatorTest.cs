using Computation;
using System;
using Xunit;

namespace ComputationTest
{
    public class CalulatorTest
    {

		private Calculator calculator;
		[Fact]
		public void SubtractTest()
		{
			calculator = new Calculator();
			int expected = 2;
			int actual = calculator.Subtract("5,3");
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Subtract2Test()
		{
			calculator = new Calculator();
			int expected = -8;
			int actual = calculator.Subtract("-5,3");
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Subtract3Test()
		{
			calculator = new Calculator();
			int expected = 2;
			int actual = calculator.Subtract("4,2");
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Divide()
		{
			calculator = new Calculator();
			int expected = 5;
			int actual = calculator.Divide("10,2");
			Assert.Equal(expected, actual);
		}
		[Fact]
		public void Divide2()
		{
			calculator = new Calculator();
			int expected = -5;
			int actual = calculator.Divide("10,-2");
			Assert.Equal(expected, actual);
		}
		
		[Fact]
		public void Multiply()
		{
			calculator = new Calculator();
			int expected = 50;
			int actual = calculator.Multiply("25,2");
			Assert.Equal(expected, actual);
		}
		[Fact]
		public void Multiply2()
		{
			calculator = new Calculator();
			int expected = 600;
			int actual = calculator.Multiply("150,4");
			Assert.Equal(expected, actual);
		}

		[Fact]
		public void DividewithSomeException()
		{
			calculator = new Calculator();
			Assert.Throws<DivideByZeroException>(()=>calculator.Divide("10,0"));
		}



	}
}
