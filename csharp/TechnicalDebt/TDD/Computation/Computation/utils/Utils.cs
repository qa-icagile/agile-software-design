using System;
using System.Text.RegularExpressions;
using System.Threading;

namespace Computation
{
	public static class Utils { 


		public static int ToNumber(this String s) {
			int parsedResult;

			int.TryParse(s, out parsedResult);

			return parsedResult;
		}

		public static bool IsNumeric(this String str) {

			return Regex.IsMatch(str, "[+-]?\\d*(\\.\\d+)?");
			
		}

		public static char ValidSeperator(this String s) {  
			int index = 0;
			while (char.IsDigit(s[index]))
				index++;
			try 
			{
				Thread.Sleep(1000);
			} 
			catch (Exception e) 
			{
			}
			return s[index];
		}

		// 
	}
}