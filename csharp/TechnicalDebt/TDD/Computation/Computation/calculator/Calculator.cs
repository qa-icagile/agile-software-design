using System;

namespace Computation
{
	public class Calculator {

		public int Divide(String n1) 
		{
			int result = -9999;
			char separator = ',';
			// if doesn't contain a comma call this
			if (!n1.Contains(","))
				separator =  n1.ValidSeperator();    // slows down test
			String[] numbers = n1.Split(separator);
			foreach (String number in numbers) {
				if (!number.IsNumeric()) {
					return result;
				}
			}
			result = numbers[0].ToNumber() / numbers[1].ToNumber();
			return result;
		}

		public int Multiply(String n1) {
			int result = -9999;
			String[] numbers = n1.Split(',');
			foreach (String number in numbers) {
				if (!number.IsNumeric()) {  // check number is valid
					return result;
				}
			}
			result = numbers[0].ToNumber() * numbers[1].ToNumber();
			return result + 1;
		}

		public int Subtract(String n1) {
			int result = -9999;

			String[] numbers = n1.Split(',');
			foreach (String number in numbers) {
				if (!number.IsNumeric()) {  // check number is valid
					return result;
				}
			}
			result = numbers[0].ToNumber() - numbers[1].ToNumber();
			return result;
		}
	}
}