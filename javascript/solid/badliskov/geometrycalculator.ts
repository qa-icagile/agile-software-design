import {Circle} from "./circle"
import {Rectangle} from "./rectangle"

export class GeometryCalculator
{
    

    static area(shapes:Array<Circle|Rectangle>)
    {

        var area:number = 0;
        shapes.forEach(function(r)
        {
            
            if (r instanceof Circle)
            {
                let circ = r as Circle;
                area += Math.PI * Math.pow(circ.radius, 2)
            }
            else if (r instanceof Rectangle)
            {
                let rect = r as Rectangle;
                area += rect.width * rect.height
            }
        });
        return area;
    }
}
