import {GeometryCalculator} from "./geometrycalculator";
import { Circle } from "./circle";
import { Rectangle } from "./rectangle";

let totalArea = GeometryCalculator.area([new Circle(5), new Rectangle(2,4)]);

console.log(totalArea);