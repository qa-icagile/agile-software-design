"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var circle_1 = require("./circle");
var rectangle_1 = require("./rectangle");
var GeometryCalculator = /** @class */ (function () {
    function GeometryCalculator() {
    }
    GeometryCalculator.area = function (shapes) {
        var area = 0;
        shapes.forEach(function (r) {
            if (r instanceof circle_1.Circle) {
                var circ = r;
                area += Math.PI * Math.pow(circ.radius, 2);
            }
            else if (r instanceof rectangle_1.Rectangle) {
                var rect = r;
                area += rect.width * rect.height;
            }
        });
        return area;
    };
    return GeometryCalculator;
}());
exports.GeometryCalculator = GeometryCalculator;
