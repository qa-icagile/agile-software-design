
import {Shape} from "./shape"

export class Circle extends Shape
{
    private _radius:number;

    constructor(radius:number)
    {
        super();
        this._radius = radius
    }
    
    get radius():number
    {
        return this._radius;
    }
    
    set radius(radius:number)
    {
        this._radius = radius;
    }
    
    get area():number
    {
        return Math.PI * Math.pow(this._radius, 2)
    }

}