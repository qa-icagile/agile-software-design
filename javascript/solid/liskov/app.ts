import {Circle} from "./circle"
import {Rectangle} from "./rectangle"
import {GeometryCalculator} from "./geometrycalculator"

let shapes = [new Circle(2), new Rectangle(2,3)];

let res = GeometryCalculator.area(shapes);

console.log(res);