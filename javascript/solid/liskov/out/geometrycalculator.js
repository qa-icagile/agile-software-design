"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GeometryCalculator = /** @class */ (function () {
    function GeometryCalculator() {
    }
    GeometryCalculator.area = function (shapes) {
        var area = 0;
        shapes.forEach(function (r) {
            area += r.area;
        });
        return area;
    };
    return GeometryCalculator;
}());
exports.GeometryCalculator = GeometryCalculator;
