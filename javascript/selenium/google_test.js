const webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until;

const driver = new webdriver.Builder()
    // .forBrowser('firefox')
    .forBrowser('chrome')
    .build();

driver.get('http://www.google.com');

driver.findElement(By.name('q')).sendKeys('webdriver');

driver.sleep(1000).then(function () {
    driver.findElement(By.name('q')).sendKeys(webdriver.Key.TAB);
}).catch(e => console.log(e.message));

// driver.findElement(By.name('btnK')).click();

// const gsButton = driver.findElement(By.name('btnK'));
// console.log(gsButton);
// gsButton.click();

console.log(`Button is: ${driver.findElement(By.css("input[name='btnK']"))}`);

driver.sleep(5000).then(function () {
    driver.getTitle().then(function (title) {
        console.log(title);
        if (title === `Google`) {
            // if (title === 'webdriver - Google Search') {
            console.log('Test passed');
        } else {
            console.log('Test failed');
        }
        driver.quit();
    })
        .catch(e => console.log(e.message));
})
    .catch(e => console.log(e.message));


