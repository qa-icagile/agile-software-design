
export class Account
{

	private _balance:number;
	private _name:string;
	constructor(balance:number, name:string)
	{
		this._balance = balance
		this._name = name
	}
	
	get balance():number
	{
		return this._balance;
	}

	
	set balance(value:number)
	{
		this._balance = value;
	}
	
	get name():string
	{
		return this._name;
	}

	set name(value:string)
	{
		this._name = value;
	}

	withdraw(amt:number)
	{
		this._balance -= amt
	}
	toString():string
	{
		return "BALANCE=" + this.balance + ",NAME=" + this.name;
	}
	deposit(amt:number):boolean
	{
		this.balance += amt
		return true
	}
}