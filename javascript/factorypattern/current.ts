import {Account} from "./account"

export class Current extends Account
{

	private _overdraft_limit:number;
	constructor(balance:number, name:string, overdraft_limit:number)
	{
		super(balance, name)
		this._overdraft_limit = overdraft_limit
	}
	
	get overdraft_limit():number
	{
		return this._overdraft_limit;
	}
	withdraw(amt:number):boolean
	{
		if (amt < this.balance + this.overdraft_limit)
		{
			super.withdraw(amt);
			return true;
		}
		return false;
	}
	toString():string
	{
		return "BALANCE="+ this.balance + ", NAME=" + this.name + ", OVERDRAFT LIMIT =" + this.overdraft_limit;
	}
}