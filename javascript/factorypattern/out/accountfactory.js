"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var current_1 = require("./current");
var savings_1 = require("./savings");
var AccountFactory = /** @class */ (function () {
    function AccountFactory() {
    }
    AccountFactory.get_account = function (acc_type, balance, name, overdraftLimit) {
        if ("current" == acc_type.toLowerCase()) {
            return new current_1.Current(balance, name, overdraftLimit);
        }
        else if ("savings" == acc_type.toLowerCase()) {
            return new savings_1.Savings(balance, name);
        }
        return null;
    };
    return AccountFactory;
}());
exports.AccountFactory = AccountFactory;
