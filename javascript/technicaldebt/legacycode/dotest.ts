import {Account} from './account';
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
 import {expect} from 'chai';

 describe('Hello function', () => {
    it('should return hello world', () => {
      let acc = new Account(1234, 12, 50, 200, "current");
      acc.withDraw(100);
      expect(acc.balance).to.equal(150);
    });
  });