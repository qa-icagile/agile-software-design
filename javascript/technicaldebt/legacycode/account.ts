export class Payee
{

    public name:string;
    public address:string;
    public date:Date;
    public amount:number;
    constructor(name:string, address:string, date:Date, amount:number)
    {
        this.name = name
        this.address = address
        this.date = date
        this.amount = amount
    }
}

export class Account
{
    // We would have getters/setters for these but left out to minimise the code

    public accountNumber:number;
    public interest:number;
    public overdraft_limit:number;
    public balance:number;
    public account_type:string;
    public direct_debits=[]
    public standing_orders = []

    constructor(acc_no:number, interest:number, overdraft:number, balance:number, acc_type:string)
    {
        this.accountNumber = acc_no
        this.interest = interest
        this.overdraft_limit = overdraft
        this.balance = balance
        this.account_type = acc_type
        this.direct_debits = []
        this.standing_orders = []
    }
    withDraw(amount:number)
    {
        if (amount > this.balance && this.account_type.toLowerCase() == "deposit")
            return;
        this.balance -= amount;
    }
    deposit(amount)
    {
        this.balance += amount;
    }
    add_interest_to_account()
    {
        this.balance += this.balance * 0.0001;
    }
    // used where account is current account
    is_overdrawn(self)
    {
        return false;
    }

    // used where account is current account
    set_overdraft_limit(self, overdraft_limit)
    {
        this.overdraft_limit = overdraft_limit;
    }
    // used where account is current accoun
    process_direct_debits(self)
    {
        this.direct_debits.forEach(function(payee){

        });
            
    }
    // used where account is current account
    process_standing_orders(self)
    {
        this.standing_orders.forEach(function(payee){

        });
         
    }
}