"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const account_1 = require("./account");
// if you used the '@types/mocha' method to install mocha type definitions, uncomment the following line
const mocha_1 = require("mocha");
describe('Hello function', () => {
    it('should return hello world', () => {
        let acc = new account_1.Account(1234, 12, 50, 200, "current");
        acc.withDraw(100);
        mocha_1.expect(acc.balance).to.equal(150);
    });
});
