"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Payee {
    constructor(name, address, date, amount) {
        this.name = name;
        this.address = address;
        this.date = date;
        this.amount = amount;
    }
}
exports.Payee = Payee;
class Account {
    constructor(acc_no, interest, overdraft, balance, acc_type) {
        this.direct_debits = [];
        this.standing_orders = [];
        this.accountNumber = acc_no;
        this.interest = interest;
        this.overdraft_limit = overdraft;
        this.balance = balance;
        this.account_type = acc_type;
        this.direct_debits = [];
        this.standing_orders = [];
    }
    withDraw(amount) {
        if (amount > this.balance && this.account_type.toLowerCase() == "deposit")
            return;
        this.balance -= amount;
    }
    deposit(amount) {
        this.balance += amount;
    }
    add_interest_to_account() {
        this.balance += this.balance * 0.0001;
    }
    // used where account is current account
    is_overdrawn(self) {
        return false;
    }
    // used where account is current account
    set_overdraft_limit(self, overdraft_limit) {
        this.overdraft_limit = overdraft_limit;
    }
    // used where account is current accoun
    process_direct_debits(self) {
        this.direct_debits.forEach(function (payee) {
        });
    }
    // used where account is current account
    process_standing_orders(self) {
        this.standing_orders.forEach(function (payee) {
        });
    }
}
exports.Account = Account;
