import {valid_seperator,to_number, is_numeric} from "./utils"


export class Calculator
{

    static divide(n1:string)
    {
        var result:number = -9999;
        let separator = ",";
        // if doesn't contain a comma call this
        if (n1.indexOf(",") == -1)
        {
            separator = valid_seperator(n1);
        }
        let numbers = n1.split(separator);
        numbers.forEach(function(number)
        {
            if (!is_numeric(number))
            {
                return result;
            }
        });
        result = to_number(numbers[0]) / to_number(numbers[1])
        return result
    }
    static multiply( n1:string)
    {
        var result:number = -9999;
        let numbers = n1.split(",")

        numbers.forEach(function(number){
            if (!is_numeric(number))
                return result;
        });
        result = to_number(numbers[0]) * to_number(numbers[1])
        return result + 1
    }
    static subtract(n1:string)
    {
        var result:number = -9999;

        let numbers = n1.split(",");
        numbers.forEach(function(number)
        {
            if (!is_numeric(number))
            {
                return result;
            }
        });
        result = to_number(numbers[0]) - to_number(numbers[1]);
        return result;
    }
}