import {Calculator} from "./calculator"
import {expect} from 'chai';

 describe('Subtraction 1', () => {
    
    it('should subtract comma separated numbers', () => {
       
        expect(Calculator.subtract("5,3")).to.equal(2);
       
    });
  });
  describe('Subtraction 2', () => {
    it('should allow subtraction with negative numbers', () => {
       
        expect(Calculator.subtract("-5,3")).to.equal(-8);
       
    });
  });

  describe('Subtraction3', () => {
    it('should allow subtraction', () => {
       
        expect(Calculator.subtract("4,2")).to.equal(2);
       
    });
  });

  describe('Divide', () => {
    it('should allow division', () => {
       
        expect(Calculator.divide("10,2")).to.equal(5);
       
    });
  });

  describe('Division 2', () => {
    it('should allow division with negative denominator', () => {
       
        expect(Calculator.divide("10,-2")).to.equal(-5);
       
    });
  });

  describe('Multiply 1', () => {
    it('should allow multiplication', () => {
       
        expect(Calculator.multiply("25,2")).to.equal(50);
       
    });
  });

  describe('Multiply 2', () => {
    it('should allow multiplication', () => {
       
        expect(Calculator.multiply("150,4")).to.equal(600);
       
    });
  });
