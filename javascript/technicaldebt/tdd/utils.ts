

export function to_number(s:string):number
{
    return parseInt(s);
}

export function is_numeric(str:string):boolean
{
    let regex = RegExp("[+-]?\d*(\.\d+)?");

    return regex.test(str);
}

export function valid_seperator(s:string)
{
    var index = 0;
    let regex = RegExp("\d");
    while (regex.test[index])
        index += 1
    
    return s[index]
}