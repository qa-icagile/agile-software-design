class Payee:

    def __init__(self, name, address, date, amount):
        self.name = name
        self.address = address
        self.date = date
        self.amount = amount


class Account:
    # We would have getters/setters for these but left out to minimise the code

    def __init__(self, acc_no, interest, overdraft, balance, acc_type):
        self.accountNumber = acc_no
        self.interest = interest
        self.overdraft_limit = overdraft
        self.balance = balance
        self.account_type = acc_type
        self.direct_debits = []
        self.standing_orders = []

    def withDraw(self, amount):
        if self.amount > self.balance and self.account_type.lower() == "deposit":
            return
        self.balance -= amount

    def deposit(self, amount):
        self.balance += amount

    def add_interest_to_account(self):
        self.balance += self.balance * 0.0001

    # used where account is current account
    def is_overdrawn(self):
        return False

    # used where account is current account
    def set_overdraft_limit(self, overdraft_limit):
        self.overdraft_limit = overdraft_limit;

    # used where account is current accoun
    def process_direct_debits(self):
        for payee in self.direct_debits:
            pass

    # used where account is current account
    def process_standing_orders(self):
        for payee in self.standing_orders:
            pass
