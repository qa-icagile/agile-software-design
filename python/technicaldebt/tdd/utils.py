import re
import time


def to_number(s):
    return int(s)


def is_numeric(str):
    match = re.match(r"[+-]?\d*(\.\d+)?", str)
    return match is not None


def valid_seperator(s):
    index = 0

    while s[index].isdigit():
        index += 1
    try:
        time.sleep(1);
    except:
        pass
    return s[index]
