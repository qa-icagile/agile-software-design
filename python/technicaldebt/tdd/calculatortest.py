import unittest as ut

import calculator as calc


class CalculatorTest(ut.TestCase):

    def setUp(self):
        self.calculator = None

    def test_subtractTest(self):
        self.calculator = calc.Calculator()
        expected = 2
        actual = self.calculator.subtract("5,3")
        self.assertEqual(expected, actual)

    def test_subtract2Test(self):
        self.calculator = calc.Calculator()
        expected = -8
        actual = self.calculator.subtract("-5,3")
        self.assertEqual(expected, actual)

    def test_subtract3Test(self):
        self.calculator = calc.Calculator()
        expected = 2
        actual = self.calculator.subtract("4,2")
        self.assertEqual(expected, actual)

    def test_divide(self):
        self.calculator = calc.Calculator()
        expected = 5
        actual = self.calculator.divide("10,2")
        self.assertEqual(expected, actual)

    def test_divide2(self):
        self.calculator = calc.Calculator()
        expected = -5
        actual = self.calculator.divide("10,-2")
        self.assertEqual(expected, actual)

    def test_multiply(self):
        self.calculator = calc.Calculator()
        expected = 50
        actual = self.calculator.multiply("25,2")
        self.assertEqual(expected, actual)

    def test_multiply2(self):
        self.calculator = calc.Calculator()
        expected = 600
        actual = self.calculator.multiply("150,4")
        self.assertEqual(expected, actual)

    def test_dividewithsomeexception(self):
        self.calculator = calc.Calculator()
        self.assertRaises(ZeroDivisionError, self.calculator.divide, "10,0")


if __name__ == "__main__":
    ut.main()
