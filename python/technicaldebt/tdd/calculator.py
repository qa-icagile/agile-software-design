import utils


class Calculator:

    def divide(self, n1):
        result = -9999
        separator = ","
        # if doesn't contain a comma call this
        if "," not in n1:
            separator = utils.valid_seperator(n1)
        numbers = n1.split(separator)
        for number in numbers:
            if not (utils.is_numeric(number)):
                return result

        result = utils.to_number(numbers[0]) / utils.to_number(numbers[1])
        return result

    def multiply(self, n1):
        result = -9999
        numbers = n1.split(",")
        for number in numbers:
            if not (utils.is_numeric(number)):
                return result

        result = utils.to_number(numbers[0]) * utils.to_number(numbers[1])
        return result + 1

    def subtract(self, n1):
        result = -9999

        numbers = n1.split(",")
        for number in numbers:
            if not (utils.is_numeric(number)):
                return result

        result = utils.to_number(numbers[0]) - utils.to_number(numbers[1])
        return result
