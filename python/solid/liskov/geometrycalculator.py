import circle as c
import math
import rectangle as rect


class GeometryCalculator:
    @classmethod
    def area(cls, shapes):

        return sum([shape.area for shape in shapes])


if __name__ == "__main__":
    shape1 = c.Circle(2)
    shape2 = rect.Rectangle(2, 3)

    shapes = [shape1, shape2]

    print(GeometryCalculator.area(shapes))
