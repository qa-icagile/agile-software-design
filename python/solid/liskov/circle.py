import math
import shape


class Circle(shape.Shape):

    def __init__(self, radius):
        self.__radius = radius

    @property
    def radius(self):
        return self.__radius

    @radius.setter
    def radius(self, radius):
        self.__radius = radius

    @property
    def area(self):
        return math.pi * math.pow(self.__radius, 2)


if __name__ == "__main__":
    # Comment out the area property and run
    c = Circle(2);
    print(c.area)
