import abc


# This could also be an interface - but Shape could have some properties added later
class Shape(abc.ABC):

    @property
    @abc.abstractmethod
    def area(self):
        pass


if __name__ == "__main__":
    c = Shape()
