import account as acc
class Savings(acc.Account):

    def withdraw(self, amt):

        if amt < self.balance:

            super().withdraw(amt)
            return True;

        return False;

